import boto3
import time
from dynamodb_json import json_util
import pandas as pd

client = boto3.client("dynamodb", region_name="us-east-1")
starttime = int(time.time())*1000
lastseen_window_size = 7
endtime = starttime - lastseen_window_size * 3600*1000
print(starttime)
print(endtime)
#  "topic, time_stamp, respiration_rate, heart_rate, gait_velocity, prediction"
result = client.query(
        TableName = "qvntra",
        KeyConditionExpression = "user_id = :user_id and time_stamp between :endtime and :starttime",
        ExpressionAttributeValues = {
            ":user_id": {"S": "thyge"},
            ":endtime": {"N": "1596642241000"},
            ":starttime": {"N": "1596667441000"}
        },
        ProjectionExpression = "topic, time_stamp, prediction",
        ScanIndexForward = False
        )
data = pd.DataFrame(json_util.loads(result["Items"]))
print(data)
#print(len(result["Items"]))