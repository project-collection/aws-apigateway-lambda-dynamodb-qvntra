import pandas as pd
import numpy as np
from helper import *
import json
import time

a = time.time()
user_id = "thyge"
client = boto3.client("dynamodb", region_name="us-east-1")
lastseen_window_size = 24
data = get_data_by_user_id(client, user_id, lastseen_window_size)

if not data.empty:

    gait_velocity = get_first_nonzero_notnull_value(data["gait_velocity"])
    heart_rate = get_first_nonzero_notnull_value(data["heart_rate"])
    respiration_rate = get_first_nonzero_notnull_value(data["respiration_rate"])

    endtime = max(data["time_stamp"])
    starttime = endtime - 1*3600*1000
    prediction = data[(data["time_stamp"]>=starttime) & (data["time_stamp"]<=endtime)]["prediction"].dropna()
    prediction_count = prediction[prediction.shift() != prediction].count()

else:
    raise(f"No Data Available for user {user_id} within last {lastseen_window_size}")