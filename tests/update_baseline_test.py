import json
import pandas as pd
import numpy as np
from helper import *
from dynamodb_json import json_util
import time
from datetime import datetime

client = boto3.client("dynamodb", region_name="us-east-1")

# get baseline_fixed user_ids

 
all_user_ids = get_all_user_ids(client)
print(all_user_ids)
# calculate the difference
user_ids = all_user_ids

starttime = int(time.time())*1000
# infinite backwards into history
window_size = 10000000000
endtime = starttime - window_size * 3600000
for user_id in user_ids:
    print(user_id)
    projection_expression = "topic, time_stamp, respiration_rate, heart_rate, gait_velocity, prediction"
    data = get_data_by_user_id(client, user_id, starttime, endtime, projection_expression)
    print(data.head())