import boto3
import pandas as pd
import numpy as np
from dynamodb_json import json_util
from datetime import datetime, timedelta
import time
import json

client = boto3.client("dynamodb", region_name="us-east-1")
user_id="thyge"
starttime = int(time.time())*1000
# lastseen_window_size = 7
lastseen_window_size = 24
endtime = starttime - lastseen_window_size * 3600 * 1000

# projection_expression = "topic, time_stamp, respiration_rate, heart_rate, gait_velocity, prediction, prediction_event"
projection_expression = "time_stamp, prediction_event, topic, heart_rate"



def get_data_by_user_id(client, 
                        user_id:str, 
                        starttime:int, 
                        endtime:int,
                        projection_expression:str,
                        expression_attribute_names:dict={}):
    """ Get data for certain user_id in time window from now to last_x_hours
    """
    paginator = client.get_paginator("query")
    page_iterator = paginator.paginate(
            TableName = "qvntra",
            KeyConditionExpression = "user_id = :user_id and time_stamp between :endtime and :starttime",
            ExpressionAttributeValues = {
                ":user_id": {"S": user_id},
                ":endtime": {"N": str(endtime)},
                ":starttime": {"N": str(starttime)}
            },
            ProjectionExpression = projection_expression,
            ScanIndexForward = False
            )
    items = []
    for page in page_iterator: items.extend(page["Items"])
    data = pd.DataFrame(json_util.loads(items))
    print(json_util.loads(items))
    return data


data = get_data_by_user_id(client, user_id, starttime, endtime, projection_expression)
print(data[data["topic"]=="sensor_bathroom_events"])