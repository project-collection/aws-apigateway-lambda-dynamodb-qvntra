import json
import pandas as pd
import numpy as np
from helper import *
from dynamodb_json import json_util
import time
from datetime import datetime
import os

def verify_parameter(user_id, period, sensor):
    if (user_id is None):
        raise ValueError("Bad Parameter: user_id")
    if period not in ("1D", "1W", "1M") or (period is None):
        raise ValueError("Bad Parameter: period")
    if sensor not in ("heart_rate", "respiration_rate", "gait_velocity", "prediction", "prediction_event", "presence")   or (sensor is None):
        raise ValueError("Bad Parameter: sensor")

def lambda_handler(event, context):
    client = boto3.client("dynamodb",
                          region_name="us-east-1")
    table_name = "qvntra-baseline-cache"
    try:
        # get parameter
        user_id = event["queryStringParameters"].get("user_id")
        period = event["queryStringParameters"].get("period")
        # heart_rate, respiratory_rate, gait_velocity
        sensor = event["queryStringParameters"].get("sensor")
        # verify parameter
        verify_parameter(user_id, period, sensor)

        # compute daily average
        if period == "1D":
            starttime = int(time.time())*1000
            projection_expression = "topic, time_stamp, respiration_rate, heart_rate, gait_velocity, prediction, prediction_event, presence"
            daily_endtime = get_beginning_of_day_unix_timestamp()
            data = get_data_by_user_id(client, user_id, starttime, daily_endtime, projection_expression)
            if sensor in ['prediction_event', 'presence']:
                avg = data[sensor].count()
            elif sensor == "gait_velocity":
                avg = data[sensor].mean() if sensor in data.columns else np.nan
            # sensor is one of [heart_rate, respiration_rate]
            else:
                avg = data[data[sensor]!=0][sensor].mean() if sensor in data.columns else np.nan
        # fetch weekly and monthly average from cache table
        elif period == "1W":
            result = client.query(
                TableName = table_name,
                KeyConditionExpression = "user_id = :user_id AND time_stamp = :time_stamp",
                ExpressionAttributeValues = {":user_id": {"S": user_id}, ":time_stamp":{"S": "0"} },
                ProjectionExpression = "user_id, time_stamp, avg_respiration_rate_weekly, avg_heart_rate_weekly, avg_gait_velocity_weekly, avg_prediction_event_weekly, avg_presence_weekly"
                )
            avg = result["Items"][0].get(f"avg_{sensor}_weekly").get("S")
        elif period == "1M":
            result = client.query(
                TableName = table_name,
                KeyConditionExpression = "user_id = :user_id AND time_stamp = :time_stamp",
                ExpressionAttributeValues = {":user_id": {"S": user_id}, ":time_stamp":{"S": "0"} },
                ProjectionExpression = "user_id, time_stamp, avg_respiration_rate_monthly, avg_heart_rate_monthly, avg_gait_velocity_monthly, avg_prediction_event_monthly, avg_presence_monthly"
                )
            avg = result["Items"][0].get(f"avg_{sensor}_monthly").get("S")

        body = {}
        body["avg"] = float(avg)

        return {
            'statusCode': 200,
            'headers': {'Content-Type': 'application/json'},
            'body': body
        }

    except Exception as e:
        print(e)
        return {
            "statusCode": 404,
            "headers": {'Content-Type': 'application/json'},
            "body": json.dumps({ "error": f"Can not get batch average for user {user_id}" }),
        }


# for local invoke testing
if __name__ == "__main__":
    event = {}
    event["queryStringParameters"] = {}
    event["queryStringParameters"]["user_id"] = "thyge"
    event["queryStringParameters"]["period"] = "1D"
    event["queryStringParameters"]["sensor"] = "heart_rate"
    response = lambda_handler(event, {})
    print(response["body"])
