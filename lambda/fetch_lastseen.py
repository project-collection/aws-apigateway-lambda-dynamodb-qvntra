import json
import boto3

ddb_resource = boto3.resource('dynamodb', region_name="us-east-1")
user_status_table = ddb_resource.Table('user_status')

    
def lambda_handler(event, context):
    user_id = event["queryStringParameters"].get("user_id")

    response = user_status_table.get_item(Key={'user_id': user_id})
    user_status = response.get('Item')

    return {
        'statusCode': 200,
        'headers': {'Content-Type': 'application/json'},
        'body': json.dumps(user_status) if user_status else ''
    }


# for local invoke testing
if __name__ == "__main__":
    event = {}
    event["queryStringParameters"] = {}
    event["queryStringParameters"]["user_id"] = "thyge"
    response = lambda_handler(event, {})
    print(response)
