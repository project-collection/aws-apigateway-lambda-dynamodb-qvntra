import boto3
from dynamodb_json import json_util

ddb_resource = boto3.resource('dynamodb', region_name="us-east-1")
user_status_table = ddb_resource.Table('user_status')


SENSOR_DATA_FIELDS = {
    'sensor_bathroom_events': ['prediction_event', 'start_timestamp', 'end_timestamp'],
    'sensor_bedroom': ['respiration_rate', 'heart_rate'],
    'sensor_gaitestimation': ['direction', 'gait_velocity'],
    'sensor_occupancy': ['presence']
}


def lambda_handler(event, context):
    raw_records = event.get("Records", [])
    update_data_per_user = {}
    init_data_per_user = {}

    for record in raw_records:
        event_type = record["eventName"]
        record_data = json_util.loads(record["dynamodb"])
        if event_type == 'INSERT':
            data = record_data['NewImage']
            user_id = data.get('user_id')

            # Fill the data with init values. We need it to optimize update
            if user_id not in init_data_per_user:
                response = user_status_table.get_item(Key={'user_id': user_id})
                init_data_per_user[user_id] = response.get('Item') or {}

            if user_id not in update_data_per_user:
                update_data_per_user[user_id] = {}

            new_sensor_data = get_sensor_data(data, update_data_per_user[user_id], init_data_per_user[user_id])
            if new_sensor_data:
                update_data_per_user[user_id].update(new_sensor_data)

    for user_id in update_data_per_user:
        user_status_table.update_item(
            Key={'user_id': user_id},
            **generate_ddb_update_kwargs(update_data_per_user[user_id])
        )


def get_sensor_data(raw_data, update_item, init_item):
    """
    Get raw sensor data and generate update fields for user status table
    :param raw_data: raw sensor record
    :param update_item: user status item with updated data through batch
    :param init_item: init user status item from db
    :return:
    """
    topic = raw_data.get('topic')

    sensor_data = update_item.get(topic) or init_item.get(topic)
    timestamp = int(raw_data.get('time_stamp', 0))

    # Since timestamp in db is in miliseconds, we need to convert python timestamp from s to ms
    if sensor_data and timestamp < int(sensor_data.get('recorded_at', 0)):
        return None

    if topic not in SENSOR_DATA_FIELDS:
        return None

    data = {field: raw_data.get(field) for field in SENSOR_DATA_FIELDS[topic]}
    data['recorded_at'] = timestamp

    return {topic: data}


def generate_ddb_update_kwargs(data):
    update_expression = []
    values = {}
    names = {}

    for key, value in data.items():
        update_expression.append(f"#{key} = :{key}")
        values[f':{key}'] = value
        names[f'#{key}'] = key

    return {
        'UpdateExpression': "SET {}".format(', '.join(update_expression)),
        'ExpressionAttributeValues': values,
        'ExpressionAttributeNames': names
    }
