#! /bin/bash

export api_endpoint=$1
echo $api_endpoint

#{
#	"gait_velocity": 0.35205322245619897,
#	"heart_rate": 68.35924369747899,
#	"respiration_rate": 7.376050420168068,
#	"prediction": 40.0
#}
function test_baseline(){
    curl "$1/baseline?user_id=thyge"
}
echo "-----------------------------Test baseline-------------------------------"
test_baseline "$api_endpoint"
echo
#{
#	"gait_velocity": 0.004227035743332299,
#	"heart_rate": 104.0,
#	"respiration_rate": 19.0,
#	"prediction": 0.0
#}
function test_lastseen(){
    curl "$1/lastseen?user_id=thyge"
}
echo "-----------------------------Test lastseen-------------------------------"
test_lastseen "$api_endpoint"
echo

function test_all(){
    # test 1D
    echo "period = 1D, sensor = heart_rate"
    curl "$1/all?user_id=thyge&period=1D&sensor=heart_rate" | head -c1000
    echo
    # test 1W
    echo "period = 1W, sensor = heart_rate"
    curl "$1/all?user_id=thyge&period=1W&sensor=heart_rate" | head -c1000
    echo
    # test 1M
    echo "period = 1M, sensor = heart_rate"
    curl "$1/all?user_id=thyge&period=1M&sensor=heart_rate" | head -c1000
    echo
}

echo "--------------------Test all--------------------------"
test_all "$api_endpoint"
echo

#4. fetch_recent_payload
function test_recent_payload(){
    curl "$1/recent_payload?user_id=thyge"
}

echo "--------------------Test recent payload--------------------------"
test_recent_payload "$api_endpoint"
echo

function test_fetch_all_average(){
    # test 1D, sensor heart_rate
    echo "period = 1D, sensor = heart_rate"
    curl "$1/all_average?user_id=thyge&period=1D&sensor=heart_rate"
    echo
    # test 1D, sensor respiration_rate
    echo "period = 1D, sensor = respiration_rate"
    curl "$1/all_average?user_id=thyge&period=1D&sensor=respiration_rate"
    echo
    # test 1D, sensor gait_velocity
    echo "period = 1D, sensor = gait_velocity"
    curl "$1/all_average?user_id=thyge&period=1D&sensor=gait_velocity"
    echo
    # test 1D, sensor presence
    echo "period = 1D, sensor = presence"
    curl "$1/all_average?user_id=thyge&period=1D&sensor=presence"
    echo
    # test 1D, sensor bathroom
    echo "period = 1D, sensor = prediction_event"
    curl "$1/all_average?user_id=thyge&period=1D&sensor=prediction_event"
    echo
    # test 1W, sensor heart_rate
    echo "period = 1W, sensor = heart_rate"
    curl "$1/all_average?user_id=thyge&period=1W&sensor=heart_rate"
    echo
    # test 1W, sensor respiration_rate
    echo "period = 1W, sensor = respiration_rate"
    curl "$1/all_average?user_id=thyge&period=1W&sensor=respiration_rate"
    echo
    # test 1W, sensor gait_velocity
    echo "period = 1W, sensor = gait_velocity"
    curl "$1/all_average?user_id=thyge&period=1W&sensor=gait_velocity"
    echo
    # test 1W, sensor presence
    echo "period = 1W, sensor = presence"
    curl "$1/all_average?user_id=thyge&period=1W&sensor=presence"
    echo
    # test 1W, sensor bathroom
    echo "period = 1W, sensor = prediction_event"
    curl "$1/all_average?user_id=thyge&period=1W&sensor=prediction_event"
    # test 1M, sensor heart_rate
    echo "period = 1M, sensor = heart_rate"
    curl "$1/all_average?user_id=thyge&period=1M&sensor=heart_rate"
    echo
    # test 1M, sensor respiration_rate
    echo "period = 1M, sensor = respiration_rate"
    curl "$1/all_average?user_id=thyge&period=1M&sensor=respiration_rate"
    echo
    # test 1M, sensor gait_velocity
    echo "period = 1M, sensor = gait_velocity"
    curl "$1/all_average?user_id=thyge&period=1M&sensor=gait_velocity"
    echo
    # test 1M, sensor presence
    echo "period = 1M, sensor = presence"
    curl "$1/all_average?user_id=thyge&period=1M&sensor=presence"
    echo
    # test 1M, sensor bathroom
    echo "period = 1M, sensor = prediction_event"
    curl "$1/all_average?user_id=thyge&period=1M&sensor=prediction_event"
}

echo "----------------------Test fetch all average----------------------"
test_fetch_all_average "$api_endpoint"
echo
